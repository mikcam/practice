from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import RetrieveModelMixin, CreateModelMixin, ListModelMixin
from apps.test.models import Test
from apps.test.serializers import TestSerializer


class TestViewSet(GenericViewSet, RetrieveModelMixin, CreateModelMixin, ListModelMixin):
    serializer_class = TestSerializer
    queryset = Test.objects.all()
